class Box :
    def __init__(self,is_open = True):
        self._contents = []
        self._open = is_open
        self._capacity = None



    def __contains__(self, machin) :
        return machin in self._contents

    def add(self, truc):
        self._contents.append(truc)


    def remove(self,machin):
        self._contents.remove(machin)


    def statut(self) :
        return self._open

    def open(self) :
        self._open = True

    def close(self) :
        self._open = False

    def look(self):
        if not self._open :
            return "La boite est fermée"
        else:
            """
            boite ouverte"""
            return "La boite contient : ".join(self._contents)

    def set_capacity(self,number) :
        self._capacity = number

    def capacity(self):
        return self._capacity

    def has_room_for(self,thing) :
        if self._capacity == None :
            return True
        else :
            save = self._capacity
            self._capacity -= thing.volume
            return save - thing.volume > 0


    def action_add(self,truc):
        if self._open and self.has_room_for(truc):
            self.add(truc)
            if self._capacity is not None :
                self._capacity -= truc.volume()
            return True
        else :
            return False

class Thing :
    def __init__(self,volume) :
        self.volume = volume

    def volume(self):
        return self._volume
