from test_tp2 import *

def test_box_create():
    b=Box()

def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")

    assert b.__contains__("truc1")
    assert not b.__contains__("oui")
    assert "truc1" in b
    assert "truc2" in b
    assert "jaime_le_pain" not in b
    assert "oui" not in b

def test_box_remove():
    b = Box()
    b.add("machin")
    b.add("oui")
    b.add("non")
    b.remove("machin")

    assert "oui" in b
    assert "non" in b
    assert not "machin" in b

def test_box_ouverte() :
    b = Box()
    assert b.statut()
    b.close()
    assert not b.statut()
    b.open
    assert not b.statut()

def action_look() :
    b = Box()
    b.add("oui")
    b.add("non")
    assert b.look() == "La boite est fermée"
    b.open()
    assert b.look() == "La boite contient : oui, non"

def test_thing_create() :
    t = Thing(3)

def test_volume_thing() :
    t = Thing(3)


def test_boite_capacity() :
    b=Box()
    b.set_capacity(5)
    assert b.capacity()== 5

def test_has_room_for():
    b= Box()
    b.set_capacity(10)
    oui = Thing(8)
    non = Thing(15)
    assert b.has_room_for(oui)
    assert not b.has_room_for(non)

def test_action_add() :
    b = Box()
    t = Thing(10000)
    assert b.action_add(t)
